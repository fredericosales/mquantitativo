#/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
Metodos Quantitativos
PGCC - UFJF
"""

# imports
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st
import seaborn as sns


class Borges(object):
    """"Dummy class borges."""

    def __init__(self, alpha, data):
        """"Dummy constructor."""
        self.alpha = alpha
        self.data = data

    def _std_dev(self):
        """"Calc std deviation."""
        return np.std(self.data)

    def _min(self):
        """Min."""
        return np.min(self.data)

    def _max(self):
        """"Max."""
        return np.max(self.data)

    def _kurtosis(self):
        """"Return Kurtosis."""
        return st.stats.kurtosis(self.data)

    def _mean(self):
        """"Return mean"""
        return np.mean(self.data)

    def _median(self):
        """"Return median"""
        return np.median(self.data)

    def _mode(self):
        """"Return mode"""
        return st.mode(self.data)

    def _variance(self):
        """"Calc variance."""
        v = st.variation(self.data)
        return np.var(self.data, ddof=1)

    def _confidence(self):
        """"Calc the interval of confidence of a dataset."""
        # res = st.t.interval(self.alpha, len(self.data) - 1, loc=np.mean(self.data), scale=st.sem(self.data))
        res = st.norm.interval(self.alpha, loc=np.mean(self.data), scale=st.sem(self.data))
        return np.around(res, decimals=2)

    def _cdf(self, name=None):
        """"Plot the freaking CDF..."""
        data_size = len(self.data)

        # set the bins edges
        data_set = sorted(set(self.data))
        bins = np.append(data_set, data_set[-1] + 1)

        # use the histogram to bin the data
        counts, bin_edges = np.histogram(self.data, bins=bins, density=False)
        counts = counts.astype(float) / data_size

        # Find the cdf
        cdf = np.cumsum(counts)

        # plot this thing
        sns.set()
        plt.plot(bin_edges[0:-1], cdf, linestyle='--', marker="o", color='b')
        plt.xlabel(name)
        plt.ylim(0, 1)
        plt.ylabel("%")
        plt.savefig('img/cdf.png')
        plt.show()
