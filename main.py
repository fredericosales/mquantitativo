#/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
Metodos Quantitativos
PGCC - UFJF
"""

# imports
from __init__ import *


if __name__ == '__main__':
    # do some
    alpha = 0.95
    data = np.array([1, 1, 2, 3, 3, 4, 5, 6, 6, 6, 7, 8, 9, 9, 9, 9])
    #print(st.describe(data))

    # alpha
    print("alpha:\t\t\t {:.2f}".format(alpha))

    # dummy class
    user = Borges(alpha, data)
    print("std deviation:\t {:.2f}".format(user._std_dev()))
    print("variance:\t\t {:.2f}".format(user._variance()))
    print("Mean:\t\t\t {:.2f}".format(user._mean()))
    print("Median:\t\t\t {:.2f}".format(user._median()))
    print("Mode:\t\t\t {}".format(user._mode()))
    print("Confidence:\t\t {}".format(user._confidence()))
    print("Kurtosis:\t\t {:.2f}".format(user._kurtosis()))
    print("Min:\t\t\t {:.2f}".format(user._min()))
    print("Max:\t\t\t {:.2f}".format(user._max()))
    user._cdf('o ovo')